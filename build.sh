#!/bin/bash

if [ -z "${IPXE_MAKE}" ] ; then
    echo "no external env using internal env"
    . ./environment
fi

cd src/

MAKE_OPTIONS=""

if [ ! -z "${IPXE_EMBED}" ] ; then
    MAKE_OPTIONS+="EMBED=${IPXE_EMBED} "
fi

if [ ! -z "${IPXE_CERT}" ] ; then
    MAKE_OPTIONS+="CERT=${IPXE_CERT} "
fi

if [ ! -z "${IPXE_TRUST}" ] ; then
    MAKE_OPTIONS+="TRUST=${IPXE_TRUST} "
fi

echo $MAKE_OPTIONS

for i in ${IPXE_MAKE//,/ }
do
    echo "make $i ..."
    rm -r $i
    make $i $MAKE_OPTIONS
done

if [ ! -z "${IPXE_EXPORT}" ] && [ -d "${IPXE_EXPORT}" ] ; then
   echo "try to export ipxe builds..."
   for i in ${IPXE_MAKE//,/ }
   do
        echo "export $i ${IPXE_EXPORT}"
        cp -r $i "${IPXE_EXPORT}"
   done
fi

exit 0
